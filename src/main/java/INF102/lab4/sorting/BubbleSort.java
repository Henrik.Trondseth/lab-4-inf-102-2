package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int end = list.size();

        for (int i = 0; i < end; i++) {
            for (int k = 1; k < end - i; k++) {
                if (list.get(k - 1).compareTo(list.get(k)) > 0) {
                    T currentItem = list.get(k - 1);
                    list.set(k - 1, list.get(k));
                    list.set(k, currentItem);

                }
            }
        }
    }
}

