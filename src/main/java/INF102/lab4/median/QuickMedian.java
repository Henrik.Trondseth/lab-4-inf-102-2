package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        
        return select(listCopy, 0, list.size(), (list.size()/2));
	}

    public <T extends Comparable<T>> T select(List<T> list, int start, int end, int middleIndex) {
        int n = end - start;
        if (n < 2)
            return list.get(start);
    
        T middleValue = list.get(list.size()/2);
    
        int nLower = 0, nSame = 0, nHigher = 0;
        int startTwo = start;
        int endTwo = end;
        while (startTwo < endTwo) {
            T first = list.get(startTwo);
            int compared = first.compareTo(middleValue);
            if (compared < 0) {
                nLower++;
                startTwo++;
            } else if (compared > 0) {
                swap(list, startTwo, --endTwo);
                if (nSame > 0)
                    swap(list, endTwo, endTwo + nSame);
                nHigher++;
            } else {
                nSame++;
                swap(list, startTwo, --endTwo);
            }
        }
        assert (nSame > 0);
        assert (nLower + nSame + nHigher == n);
        assert (list.get(start + nLower) == middleValue);
        assert (list.get(end - nHigher - 1) == middleValue);
        if (middleIndex >= n - nHigher)
            return select(list, end - nHigher, end, middleIndex - nLower - nSame);
        else if (middleIndex < nLower)
            return select(list, start, start + nLower, middleIndex);
        return list.get(start + middleIndex);
    }

    private <T extends Comparable<T>> void swap(List<T> list, int startTwo, int i) {
        T item = list.get(startTwo);
        T itemTwo = list.get(i);
        list.set(i, item);
        list.set(startTwo, itemTwo);
    }


}


